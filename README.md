## INTRODUCTION

The Table Wrappers module is a Wraps tables in content fields with a custom div.

The primary use case for this module is:

Add a custom div every table on content

## REQUIREMENTS

N/A

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
NO CONFIGURATION JUST INSTALL

## MAINTAINERS

Current maintainers for Drupal 10:

- Rusyl Narito (Rus) - https://www.drupal.org/u/rusylnarito

